<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after
 *
 * @package understrap
 */

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;

?>


<div id="footer" class="section">
	<div class="container">
		<div class="row">
			<div class="col-md-3 col-xs-12">
				<div class="footer-logo text-center">
					<?php
						if ( is_active_sidebar( 'footer-column1' ) ) {
							dynamic_sidebar( 'footer-column1' );  
						}
					?>
				</div>
			</div>
			<div class="col-md-3 col-xs-12">
				<div class="footer-qlinks">
					<?php
						if ( is_active_sidebar( 'footer-column12' ) ) {
							dynamic_sidebar( 'footer-column12' );  
						}
					?>
					<?php echo do_shortcode("[latesttoolsadvice]"); ?>
				</div>
			</div>
			<div class="col-md-2 col-xs-12">
				<div class="footer-contact">
					<?php
						if ( is_active_sidebar( 'footer-column2' ) ) {
							dynamic_sidebar( 'footer-column2' );  
						}
					?>
				</div>
			</div>
			<div class="col-md-4 col-xs-12">
				<div class="footer-subscription">
					<?php
						if ( is_active_sidebar( 'footer-column3' ) ) {
							dynamic_sidebar( 'footer-column3' );  
						}
					?>
				</div>
			</div>
			<div class="col-12 mb-5 mt-4">
				<div class="divider"></div>
			</div>
			<div class="col-12 text-center footer-text mb-5">
				<?php
						if ( is_active_sidebar( 'footer-fulltext' ) ) {
							dynamic_sidebar( 'footer-fulltext' );  
						}
				?>
			</div>
			<div class="col-12 text-center copyright-text">
				<?php
						if ( is_active_sidebar( 'footer-copytext' ) ) {
							dynamic_sidebar( 'footer-copytext' );  
						}
				?>
			</div>

		</div>
	</div>
</div>



<?php wp_footer(); ?>


<script type="text/javascript">
jQuery(window).scroll(function(){
    if (jQuery(window).scrollTop() >= 168) {
        jQuery('#mainmenu').addClass('fixed-header');
    }
    else {
        jQuery('#mainmenu').removeClass('fixed-header');
    }
});
</script>

</body>

</html>

