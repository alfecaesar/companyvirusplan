<?php
/**
 * The template for displaying all single posts
 *
 * @package understrap
 */

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;

get_header();
$container = get_theme_mod( 'understrap_container_type' );
?>

<div class="wrapper" id="single-wrapper">

	<div class="<?php echo esc_attr( $container ); ?>" id="content" tabindex="-1">

		<div class="row">


			<main class="site-main" id="main">

				<?php while ( have_posts() ) : the_post(); ?>

					<?php get_template_part( 'loop-templates/content', 'single' ); ?>

					<?php understrap_post_nav(); ?>

					<div class="sign-up-box">
						<h4>Stay Informed About the Latest Insights</h4>
						<div class="row">
							<div class="col-9 form-group">
								<input type="text" name="email" class="form-control" placeholder="Enter Email Address">
							</div>
							<div class="col-3 form-group">
								<input type="submit" class="form-control" value="Subscribe">
							</div>
						</div>
					</div>
				
					<?php
					// If comments are open or we have at least one comment, load up the comment template.
					if ( comments_open() || get_comments_number() ) :
						comments_template();
					endif;
					?>
				
				
					

					<?php echo do_shortcode('[elementor-template id="278"]'); ?>
				
					<?php 
						$cat = get_the_category(); 
						$catname = $cat[0]->cat_name;
						//echo $catname;
						if($catname === 'Manage the Work Environment and Facilities'){ ?>
							<style type="text/css">
								#blog_cat_1{display: none !important;}
								#services .elementor-row {
									max-width: 755px;
									margin: 0 auto;
								}
							</style>
							<script type="text/javascript">
								jQuery('#services #blog_cat_2').removeClass('elementor-col-33').addClass('elementor-col-50');
								jQuery('#services #blog_cat_3').removeClass('elementor-col-33').addClass('elementor-col-50');
							</script>
						<?php }elseif($catname === 'Develop Policies and Procedures'){  ?>
							<style type="text/css">
								#blog_cat_2{display: none !important;}
								#services .elementor-row {
									max-width: 755px;
									margin: 0 auto;
								}
							</style>
							<script type="text/javascript">
								jQuery('#services #blog_cat_1').removeClass('elementor-col-33').addClass('elementor-col-50');
								jQuery('#services #blog_cat_3').removeClass('elementor-col-33').addClass('elementor-col-50');
							</script>
						<?php }elseif($catname === 'Guide Employees'){  ?>
							<style type="text/css">
								#blog_cat_3{display: none !important;}
								#services .elementor-row {
									max-width: 755px;
									margin: 0 auto;
								}
							</style>
							<script type="text/javascript">
								jQuery('#services #blog_cat_1').removeClass('elementor-col-33').addClass('elementor-col-50');
								jQuery('#services #blog_cat_2').removeClass('elementor-col-33').addClass('elementor-col-50');
							</script>
						<?php }
					?>
				
				</div>

				<?php endwhile; // end of the loop. ?>

			</main><!-- #main -->

		</div><!-- .row -->

	</div><!-- #content -->

</div><!-- #single-wrapper -->

<?php get_footer();
