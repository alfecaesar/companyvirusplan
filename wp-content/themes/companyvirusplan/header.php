<?php
/**
 * The header for our theme
 *
 * Displays all of the <head> section and everything up till <div id="content">
 *
 * @package understrap
 */

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;

$container = get_theme_mod( 'understrap_container_type' );
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link href="https://fonts.googleapis.com/css?family=Josefin+Sans:300,400,500,600,700&display=swap" rel="stylesheet">
	<link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/css/all.min.css" rel="stylesheet">
	<?php wp_head(); ?>
	<link rel='stylesheet' id='custom-dev-css'  href='<?php echo get_template_directory_uri(); ?>/css/customdev.css?v=<?php echo mt_rand(0001, 9999) ?>' type='text/css' media='all' />
</head>

<body <?php body_class(); ?> <?php understrap_body_attributes(); ?>> 
<?php do_action( 'wp_body_open' ); ?>
<div class="site" id="page">

	<!-- ******************* The Navbar Area ******************* -->
	<div id="topbar" class="section">
		<div class="container">
			<div class="row">
				<div class="col-md-9 col-6 col-l">
					<?php
						if ( is_active_sidebar( 'topbar-leftcolumn' ) ) {
							dynamic_sidebar( 'topbar-leftcolumn' );  
						}
					?>
				</div>
				<div class="col-md-3 col-6 col-r">
					<?php
						if ( is_active_sidebar( 'topbar-rightcolumn' ) ) {
							dynamic_sidebar( 'topbar-rightcolumn' );  
						}
					?>
				</div>
			</div>
		</div>
	</div>

	<div id="mainmenu" class="section <?php if (is_front_page()) echo 'home'; ?>">
		<div class="container">
			<div class="row">
				<div class="col-md-3 col-xs-12 logo">
					<?php the_custom_logo(); ?>
				</div>
				<div class="col-md-9 col-xs-12 mainnav">
					<?php wp_nav_menu(
						array(
							'theme_location'  => 'primary',
							'container_class' => '',
							'container_id'    => '',
							'menu_class'      => '',
							'fallback_cb'     => '',
							'menu_id'         => 'main-nav',
							'depth'           => 2,
							'walker'          => new Understrap_WP_Bootstrap_Navwalker(),
						)
					); ?>
				</div>
			</div>
		</div>
	</div>	



	