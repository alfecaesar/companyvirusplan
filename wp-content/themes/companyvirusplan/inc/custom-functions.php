<?php

function hide_admin_bar(){ return false; }
add_filter( 'show_admin_bar', 'hide_admin_bar' );


    add_filter( 'get_the_archive_title', function ($title) {    
        if ( is_category() ) {    
                $title = single_cat_title( '', false );    
            } elseif ( is_tag() ) {    
                $title = single_tag_title( '', false );    
            } elseif ( is_author() ) {    
                $title = '<span class="vcard">' . get_the_author() . '</span>' ;    
            } elseif ( is_tax() ) { //for custom post types
                $title = sprintf( __( '%1$s' ), single_term_title( '', false ) );
            } elseif (is_post_type_archive()) {
                $title = post_type_archive_title( '', false );
            }
        return $title;    
    });

/* Footer Latest Tools and Advice */
function displaylatesttoolsadvice(){

		$args = array(
			'post_type'   => 'post',
			'posts_per_page' => 3, 
			'cat' => 9,
		);


        ?>
        <ul class="row">
        <?php
		$latesttools = new WP_Query( $args );
        if($latesttools->have_posts()){
            while( $latesttools->have_posts() ) : 
			$latesttools->the_post();
                ?>            
			<li><a href="<?php echo get_permalink(); ?>"><?php echo get_the_title(); ?></a></li>
                <?php
            endwhile;
            wp_reset_postdata();
        } 
        ?>
        </ul>

        <?php
}
add_shortcode( 'latesttoolsadvice', 'displaylatesttoolsadvice' );
// [latesttoolsadvice]


function displaykeepingworkplacesafe(){

		$args = array(
			'post_type'   => 'post',
			'posts_per_page' => 6, 
			'cat' => 6,
		);


        ?>
        <ul class="row">
        <?php
		$latesttools = new WP_Query( $args );
        if($latesttools->have_posts()){
            while( $latesttools->have_posts() ) : 
			$latesttools->the_post();
                ?>            
			<li><a href="<?php echo get_permalink(); ?>"><?php echo get_the_title(); ?></a></li>
                <?php
            endwhile;
            wp_reset_postdata();
        } 
        ?>
        </ul>

        <?php
}
add_shortcode( 'latestkeepingworkplacesafe', 'displaykeepingworkplacesafe' );
// [latestkeepingworkplacesafe]


function displaycompanypolicies(){

		$args = array(
			'post_type'   => 'post',
			'posts_per_page' => 6, 
			'cat' => 7,
		);


        ?>
        <ul class="row">
        <?php
		$latesttools = new WP_Query( $args );
        if($latesttools->have_posts()){
            while( $latesttools->have_posts() ) : 
			$latesttools->the_post();
                ?>            
			<li><a href="<?php echo get_permalink(); ?>"><?php echo get_the_title(); ?></a></li>
                <?php
            endwhile;
            wp_reset_postdata();
        } 
        ?>
        </ul>

        <?php
}
add_shortcode( 'latestcompanypolicies', 'displaycompanypolicies' );
// [latestcompanypolicies]


function displayguidingemployees(){

		$args = array(
			'post_type'   => 'post',
			'posts_per_page' => 6, 
			'cat' => 8,
		);


        ?>
        <ul class="row">
        <?php
		$latesttools = new WP_Query( $args );
        if($latesttools->have_posts()){
            while( $latesttools->have_posts() ) : 
			$latesttools->the_post();
                ?>            
			<li><a href="<?php echo get_permalink(); ?>"><?php echo get_the_title(); ?></a></li>
                <?php
            endwhile;
            wp_reset_postdata();
        } 
        ?>
        </ul>

        <?php
}
add_shortcode( 'latestguidingemployees', 'displayguidingemployees' );
// [latestguidingemployees]