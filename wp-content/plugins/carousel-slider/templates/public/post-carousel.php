<?php
// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

$_image_size       = get_post_meta( $id, '_image_size', true );
$_nav_color        = get_post_meta( $id, '_nav_color', true );
$_nav_active_color = get_post_meta( $id, '_nav_active_color', true );
$_lazy_load_image  = get_post_meta( $id, '_lazy_load_image', true );
?>
<div class="carousel-slider-outer carousel-slider-outer-posts carousel-slider-outer-<?php echo $id; ?>">
	<?php carousel_slider_inline_style( $id ); ?>
    <div <?php echo join( " ", $this->carousel_options( $id ) ); ?>>
		<?php
		$posts = carousel_slider_posts( $id );
		foreach ( $posts as $_post ):
			global $post;
			$post = $_post;
			setup_postdata( $post );
			$category = get_the_category( $post->ID );

			do_action( 'carousel_slider_post_loop', $post, $category );

			$html = '<div class="carousel-slider__post">';
			$html .= '<div class="carousel-slider__post-content">';
			$html .= '<div class="carousel-slider__post-header text-center">';
			// Post Thumbnail
			$_permalink = esc_url( get_permalink( $post->ID ) );
			$_thumb_id  = get_post_thumbnail_id( $post->ID );
			$num_words  = apply_filters( 'carousel_slider_post_excerpt_length', 20 );
			$more_text  = apply_filters( 'carousel_slider_post_read_more', ' ...', $post );
			$_excerpt   = wp_trim_words( wp_strip_all_tags( $post->post_content ), $num_words, $more_text );

			$image_src = wp_get_attachment_image_src( $_thumb_id, $_image_size );

			$html .= sprintf( '<img src="%s" alt="#" class="download-img" />',$image_src[0]);
			$html .= sprintf( '<a class="btn" href="%s">Download</a>',$image_src[0]);
			$html .= '</div>'; // End Post Header
			$html .= '</div>';
			$html .= '</div>';

			echo apply_filters( 'carousel_slider_post', $html, $post, $category );
		endforeach;
		wp_reset_postdata();
		?>
    </div>
</div>